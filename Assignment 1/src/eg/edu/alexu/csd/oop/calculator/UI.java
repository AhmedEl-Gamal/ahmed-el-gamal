package eg.edu.alexu.csd.oop.calculator;
import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;

import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

    public class UI extends JFrame {
	private JTextField display;
	private JButton one,two,three,four,five,six,seven,eight,nine;
	private JButton prev,next,current;
	private JButton plus,minus,multiply,divide;
	private JButton delete,backspace,result;
	private JButton save,load;
	
	private String receive="",answer="0";
	private String entry="";
	private JLabel outcome,statement; 	

	Engine engine = Engine.getInstance();
	public class Command implements ActionListener{
		public Command(){
			one.addActionListener(this);	
			two.addActionListener(this);
			three.addActionListener(this);
			four.addActionListener(this);
			five.addActionListener(this);	
			six.addActionListener(this);
			seven.addActionListener(this);
			eight.addActionListener(this);
			nine.addActionListener(this);	
			result.addActionListener(this);
			prev.addActionListener(this);
			next.addActionListener(this);
			current.addActionListener(this);
			plus.addActionListener(this);
			minus.addActionListener(this);
			multiply.addActionListener(this);
			divide.addActionListener(this);
			save.addActionListener(this);	
			load.addActionListener(this);
			backspace.addActionListener(this);
			delete.addActionListener(this);
			display.addActionListener(this);
		}
		
		@Override
		public void actionPerformed(ActionEvent evt){
			Object src = evt.getSource();

			if (src == one) {
				receive += "1";
			}
			else if (src == two) {
				receive += "2";
			}
			else if (src == three) {
				receive += "3";
			}
			else if (src == four) {
				receive += "4";
			}
			else if (src == five) {
				receive += "5";
			}
			else if (src == six) {
				receive += "6";
			}
			else if (src == seven) {
				receive += "7";
			}
			else if (src == eight) {
				receive += "8";
			}
			else if (src == nine) {
				receive += "9";
			}
			else if (src == plus) {
				receive += "+";
			}
			else if (src == minus) {
				receive += "-";
			}
			else if (src == multiply) {
				receive += "*";
			}
			else if (src == divide) {
				receive += "/";
			}
			else if (src == backspace) {
				entry = display.getText();
				receive = "";
				for(int i=0;i<entry.length()-1;i++)
					receive += entry.charAt(i);
			}
			else if (src == delete) {
				receive = "";
				answer="0";
			}
			else if (src == prev) {
				receive = engine.prev();
				if (receive == null)
					receive = "No prev Operations";
			}
			else if (src == next) {
				receive = engine.next();				
				if (receive == null)
					receive = "No more Operations";
			}
			else if (src == current) {
				receive = engine.current();
				if (receive == null)
					receive = "History is empty";
			}
			
			else if (src == display){
				entry = display.getText();
				receive = entry;
				if(entry.equals("No prev Operations") || entry.equals("No more Operations") || entry.equals("History is Empty") || entry.equals("Division by zero") || entry.equals("No Saved File !")){
					entry = "0";
					answer = "";
				}
				else{
					engine.input(entry);
					try{
						answer = engine.getResult();
					}catch(RuntimeException e){
						receive = "No Input Exists";
						answer = "0";
					}
					for(int i=0;i<entry.length()-1;i++)
						if(entry.charAt(i)=='/' && entry.charAt(i+1)=='0')
							receive = "Division by zero";
				}
			}
			
			else if (src == result) {
				entry = display.getText();
				receive = engine.current();
				try{
					answer = engine.getResult();
				}catch(RuntimeException e){
					receive = "No Input exists";
					answer = "0";
				}
				for(int i=0;i<entry.length()-1;i++)
					if(entry.charAt(i)=='/' && entry.charAt(i+1)=='0')
						receive = "Division by zero";
			}
			else if (src == save) {
				engine.save();
			}
			else if (src == load) {
				try{
					engine.load();
				}catch(RuntimeException e){
					receive = "No Saved File !";
					answer = "";
				}
				
			}
			display.setText(receive);
			if (receive.equals(""))
				display.setText("0");
			outcome.setText(answer);
		}
	}

	public static void main(String[] args) {
		try{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e){
			System.out.println("Could not load system interface\n");
		}
		new UI();
	}
	
	public UI(){
		
		super("UI");
		sendDisplay();		
		sendLabels();
		sendButtons();
		sendUI(this);
	}
	
	private void sendButtons() {
		one = new JButton("1");
		one.setBounds(10, 450, 50, 60);
		one.setVisible(true);
		add(one);		
		
		two = new JButton("2");
		two.setBounds(70,450,50,60);
		add(two);

		three = new JButton("3");
		three.setBounds(130,450,50,60);
		add(three);
		
		four = new JButton("4");
		four.setBounds(10,380,50,60);
		add(four);
		
		
		five = new JButton("5");
		five.setBounds(70,380,50,60);
		add(five);
		
		six = new JButton("6");
		six.setBounds(130,380,50,60);
		add(six);
		
		seven = new JButton("7");
		seven.setBounds(10,310,50,60);
		add(seven);
		
		eight = new JButton("8");
		eight.setBounds(70,310,50,60);
		add(eight);
		
		nine = new JButton("9");
		nine.setBounds(130,310,50,60);
		add(nine);
		
		prev = new JButton("Prev");
		prev.setBounds(260,310,120,60);
		add(prev);
		
		next = new JButton("Next");
		next.setBounds(260,380,120,60);
		add(next);
		
		current = new JButton("Current");
		current.setBounds(260,450,120,60);
		add(current);

		plus = new JButton("+");
		plus.setBounds(200,310,50,50);
		add(plus);
		
		minus = new JButton("-");
		minus.setBounds(200,370,50,50);
		add(minus);
		
		multiply = new JButton("*");
		multiply.setBounds(200,430,50,50);
		add(multiply);
		
		divide = new JButton("/");
		divide.setBounds(200,490,50,50);
		add(divide);
		
		save = new JButton("Save");
		save.setBounds(10,520,80,40);
		add(save);
		
		load = new JButton("Load");
		load.setBounds(100,520,80,40);
		add(load);
		
		delete = new JButton("AC");
		delete.setBounds(10,240,60,40);
		add(delete);
		
		backspace = new JButton("Backspace");
		backspace.setBounds(80,240,120,40);
		add(backspace);

		result = new JButton("=");
		result.setBounds(260,240,120,40);
		add(result);

		Command command = new Command();
	}

	private void sendDisplay() {
		display = new JTextField("0");
		display.setBounds(10,10,324,50);
		display.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		display.setEditable(true);
		display.setFont(new Font("Arial",Font.PLAIN,30));
		display.setVisible(true);
		add(display);
	}

	private void sendLabels() {
		statement = new JLabel("Result");
		statement.setBounds(20,80,120,50);
		statement.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		statement.setFont(new Font("Arial",Font.PLAIN,25));
		statement.setVisible(true);
		add(statement);
		
		outcome = new JLabel(answer);
		outcome.setBounds(150,80,120,50);
		outcome.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		outcome.setFont(new Font("Arial",Font.PLAIN,25));
		outcome.setVisible(true);
		add(outcome);
	}

	private void sendUI(UI ui) {
	    ui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    ui.setSize(400,600);
	    ui.setLayout(null);
	    ui.setResizable(false);
	    ui.setLocationRelativeTo(null);
	    ui.setVisible(true);               
	}
}