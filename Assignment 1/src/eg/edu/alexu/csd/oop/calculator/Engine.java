package eg.edu.alexu.csd.oop.calculator;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

import javax.script.ScriptException;

public class Engine implements Calculator{
	private int pointer=-1,size=0;
	private final int MAXIMUM=5;
	private String answer=""; 
	private String input="";
	
	Parser parser = new Parser();
	
	LinkedList<String> history = new LinkedList<String>();

	private static Engine ENGINE;
	private Engine(){
		this.history.clear();
		this.size=0;
		this.pointer=-1;
		this.save();
		try{
    		File file = new File("history.txt");
    		if(file.delete()){
    			System.out.println(file.getName() + " is deleted!");
    		}else{
    			System.out.println("Delete operation is failed.");
    		}
    	}catch(Exception e){}
	}
	public static Engine getInstance(){
		if(ENGINE == null)
			ENGINE = new Engine();
		return ENGINE;
	}

	public static void destoryInstance(){
        ENGINE = null;
	}
	
	@Override
	public void input(String expression){
		input = expression;
		
		//to update history
		for(int i=pointer+1;i<history.size();i++)
			history.remove(i);

		size = history.size();
		if(size == MAXIMUM){
			history.removeFirst();
			history.addLast(input);
		}
		else{
			history.addLast(input);
		}
		pointer=history.size()-1;
	}

	@Override
	public String getResult(){
		if(pointer == -1)
			throw new RuntimeException();
		String temp = history.get(pointer);
		int length = temp.length();
		try {
			answer = parser.get_answer(history.get(pointer));
		}catch (ScriptException e) {
			throw new RuntimeException();
		}
		if(answer.equals("Infinity"))
			throw new RuntimeException();
		
		return answer;
	}
	
	
	@Override
	public String current(){
		size = history.size();
		if(size==0)
			return null;
		else
			return history.get(pointer);
	}
	
	@Override
	public String prev(){
		if(pointer == 0 || pointer == -1){
			input = null;
			return null;
		}
		else
			pointer--;
		
		answer = history.get(pointer);
		input = answer;
		return answer;
	}
	
	@Override
	public String next(){
		size = history.size();
		if(pointer == size-1){
			input = null;
			return null;
		}
		else
			pointer++;
		answer = history.get(pointer);
		input = answer;
		return answer;
	}

	@Override
	public void save() {
		try {
			File file = new File("history.txt");

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			size = history.size();
			System.out.println(pointer);
			for(int i=0;i<=pointer;i++){
				if(history.get(i)==null)
					break;
				else 
					bw.write(history.get(i)+"\n");
			}
			bw.close();
			System.out.println("Done");

		}catch (IOException e) {
		}
	}
	
	@Override
	public void load() {
		this.history.clear();
		this.size=0;
		this.pointer=-1;
		FileInputStream fstream = null;
		try {
			fstream = new FileInputStream("history.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String strLine="";
			while ((strLine = br.readLine()) != null)   {
				history.add(strLine);
			}
			size = history.size();
			pointer = size-1;
			br.close();
		}catch (FileNotFoundException e) {
			throw new RuntimeException();
		}
		catch (IOException e) {}
	}

}