package eg.edu.alexu.csd.oop.calculator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class JUnit {
	
	Engine c = Engine.getInstance();
	@Before 
	public void initialize() {
        c.destoryInstance();
	}
	
	@ Test
	public void testCurrentPrevNext() {
		c.input("5656");
		c.input("5+6");
		c.input("3+4");
		c.input("2+2");
		c.input("8+5");
		assertEquals("8+5", c.current());
		assertEquals(null, c.next());
		assertEquals("2+2", c.prev());
		assertEquals(Double.parseDouble("4.0"),
				Double.parseDouble(c.getResult()), 1e-19);
		assertEquals("3+4", c.prev());
		assertEquals("5+6", c.prev());
		assertEquals(null, c.prev());
		assertEquals("3+4", c.next());
		assertEquals("5+6", c.prev());
		assertEquals(null, c.prev());
		c.input("5+6");
		c.input("3+4");
		c.input("2+2");
		c.input("8+5");
		c.input("9+6");
		c.input("8+19");
		assertEquals(null, c.next());
		for (int counter = 0; counter < 5; counter++) {
			c.prev();
		}
		assertEquals(null, c.prev());
		c.input("fff + 5");
		try{
			c.getResult();
			fail("had to crash");
		}catch(RuntimeException e){}
	}
	
	@Test
	public void testSaveLoad() {
		c.destoryInstance();
		try {
			c.load();
			//fail("had to crash");
		} catch (RuntimeException e) {
			System.out.println("was here");

		}
		c.save();
		try {
			c.load();
		} catch (RuntimeException e) {
			System.out.println("again was here");
			fail("wrong craSh :D :D");
		}
		c.input("5+6");
		c.input("3+4");
		c.input("2+2");
		c.input("8+5");
		c.input("7+6");
		c.load();
		assertEquals(null, c.current());
		c.input("5+6");
		c.input("3+4");
		c.input("2+2");
		c.input("8+5");
		c.input("7+6");
		c.input("3+3");
		c.save();
		c.load();
		assertEquals("3+3", c.current());
		assertEquals(null, c.next());
		assertEquals("7+6", c.prev());
		c.input("3");
		c.save();
		assertEquals(null, c.next());
		c.load();
		assertEquals("3", c.current());
	}
}