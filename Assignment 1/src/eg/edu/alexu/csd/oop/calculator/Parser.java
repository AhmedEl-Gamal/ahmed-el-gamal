package eg.edu.alexu.csd.oop.calculator;

import javax.script.ScriptEngineManager;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

public class Parser {
	private String answer=""; 
	public String get_answer(String Math_Expression) throws ScriptException{
	    ScriptEngineManager mgr = new ScriptEngineManager();
	    ScriptEngine engine = mgr.getEngineByName("JavaScript");
	    answer = (engine.eval(Math_Expression).toString());
	    return answer;
	}
}
